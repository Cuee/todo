import sqlite3
from todo_connect import create_connection

connection = create_connection()
cursor = connection.cursor()

def get_row_count():
    try:
        query = ''' SELECT * FROM user'''
        execute_query = cursor.execute(query).fetchall()
        row_count = len(execute_query)
        return row_count
    except Exception as exception:
        return exception

def add_user(username):
    try:
        user_id = get_row_count()
        if (check_if_user_exists(username)):
            print("User Exists")
            return "User Exists"
        elif(check_if_user_exists(username) == False):
            query = '''INSERT INTO user VALUES(?, ?)'''
            execute_query = cursor.execute(query, (user_id, username, ))
            connection.commit()
            connection.close()
            print("User Created")
            return "User Created"
        else:
            return "Error"
    except Exception as exception:
        return exception


def check_if_user_exists(username):
    try:
        query = ''' SELECT id FROM user WHERE username=?'''
        execute_query = cursor.execute(query, (username,)).fetchone()
        if execute_query == None:
            return False
        else:
            return True
    except Exception as exception:
        return exception

def get_user_id(username):
    try:
        if check_if_user_exists(username):
            query = ''' SELECT id FROM user WHERE username=?'''
            execute_query = cursor.execute(query, (username, )).fetchone()
            user_id = execute_query[0]
            return user_id
        elif check_if_user_exists(username) == False:
            return -1
    except Exception as exception:
        return exception

def get_username(user_id):
    try:
        query = ''' SELECT username FROM user WHERE id=?'''
        execute_query = cursor.execute(query, (user_id, )).fetchone()
        print(execute_query)
        return execute_query
    except Exception as exception:
        return exception
    
    
# print(get_user_id('Evans'))

# add_user('Evans')
