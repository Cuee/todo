import sqlite3
from todo_connect import create_connection
from user import get_user_id
from category import get_category_id
from datetime import date

connection = create_connection()
cursor = connection.cursor()

def get_row_count():
    try:
        query = ''' SELECT * FROM todo'''
        execute_query = cursor.execute(query).fetchall()
        row_count = len(execute_query)
        return row_count
    except Exception as exception:
        return exception

def add_todo(username, category, title, content):
    try:
        todo_id = get_row_count()
        user_id = get_user_id(username)
        category_id = get_category_id(category, username)
        if (check_if_todo_is_valid(category, username, title) == True):
            query = '''INSERT INTO todo VALUES(?, ?, ?, ?, ?, ?)'''
            execute_query = cursor.execute(query, (todo_id, title, content, user_id, category_id, date.today(), ))
            connection.commit()
            return "Todo Created"
        elif(check_if_todo_is_valid(category, username, title) == False):
            return "Invalid Todo"
        else:
            return "Error"
    except Exception as exception:
        return exception


def check_if_todo_is_valid(category, username, title):
    try:
        user_id = get_user_id(username)
        category_id = get_category_id(category, username)
        query = ''' SELECT id FROM todo WHERE createdAt=? AND user_id=? AND category_id=? AND title=? '''
        execute_query = cursor.execute(query, (date.today(), user_id, category_id, title, )).fetchall()
        todo_count = len(execute_query)
        if todo_count == 0:
            return True
        elif todo_count > 0:
            return False
        else:
            return "Error"
    except Exception as exception:
        return exception

def get_todos(username):
    try:
        user_id = get_user_id(username)
        query = ''' SELECT title, content, createdAt, category_id FROM todo WHERE user_id=?'''
        execute_query = cursor.execute(query, (user_id, )).fetchall()
        return execute_query
    except Exception as exception:
        return exception

# add_todo('Chieto', 'Machine Learning', 'Dancing', 'Dance Galala')
# get_todos('Chieto')
# add_todo('Chieto', 'Machine Learning', 'Dancing', 'Dance Galala')