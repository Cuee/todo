import sqlite3
from todo_connect import create_connection
from user import get_user_id
from category import get_category_name, get_categories
from todo import get_todos


def get_user_details(username):
    try:
        todos = []
        categories = []
        user_details = []
        user_categories = get_categories(username)
        user_todos = get_todos(username)
        for item in user_todos:
            todos.extend(item)
        for category in user_categories:
            categories.extend(category)
        user_details.append(categories)
        user_details.append(todos)
        return user_details
    except Exception as exception:
        return exception
    

def get_category_by_name(category_id):
    try:
        category_name = get_category_name(category_id)
        return category_name
    except Exception as exception:
        return exception


# print(get_user_details('Chieto'))