import sqlite3
from todo_connect import create_connection
from user import get_user_id

connection = create_connection()
cursor = connection.cursor()

def get_row_count():
    try:
        query = ''' SELECT * FROM category'''
        execute_query = cursor.execute(query).fetchall()
        row_count = len(execute_query)
        return row_count
    except Exception as exception:
        return exception

def add_category(username, name):
    try:
        category_id = get_row_count()
        user_id = get_user_id(username)
        if (check_if_category_exists(user_id, name)):
            return "Category Exists"
        elif(check_if_category_exists(user_id, name) == False):
            query = '''INSERT INTO category VALUES(?, ?, ?)'''
            execute_query = cursor.execute(query, (category_id, name, user_id, ))
            connection.commit()
            connection.close()
            return "Category Created"
        else:
            return "Error"
    except Exception as exception:
        return exception


def check_if_category_exists(user_id, name):
    try:
        query = ''' SELECT name FROM category WHERE  user_id=? AND name=?'''
        execute_query = cursor.execute(query, (user_id, name, )).fetchall()
        count = len(execute_query)
        if count == 0:
            return False
        elif count > 0:
            return True
    except Exception as exception:
        return exception

def get_category_id(category, username):
    try:
        user_id = get_user_id(username)
        if check_if_category_exists(user_id, category):
            query = ''' SELECT id FROM category WHERE name=?'''
            execute_query = cursor.execute(query, (category, )).fetchone()
            category_id = execute_query[0]
            return category_id
        elif check_if_category_exists(user_id, category) == False:
            return "Category does not exist"
    except Exception as exception:
        return exception

def get_categories(username):
    try:
        user_id = get_user_id(username)
        query = ''' SELECT name FROM category WHERE user_id=?'''
        execute_query = cursor.execute(query, (user_id, )).fetchall()
        return execute_query
    except Exception as exception:
        return exception

def get_category_name(category_id):
    try:
        query = ''' SELECT name FROM category WHERE id=?'''
        execute_query = cursor.execute(query, (category_id, )).fetchone()
        return execute_query
    except Exception as exception:
        return exception


# add_category('Chieto', 'Machine Learning')
