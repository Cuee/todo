import sqlite3
from todo_connect import create_connection

connection = create_connection()


def check_for_category_table():
    try:
        query = (''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='category' ''')
        execute_query = connection.execute(query).fetchone()
        if execute_query[0] == 1:
            return True
        else:
            return False
        connection.commit()
    except Exception as exception:
        print("Errorcater: ", exception)

def check_for_todo_table():
    try:
        query = (''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='todo' ''')
        execute_query = connection.execute(query).fetchone()
        if execute_query[0] == 1:
            return True
        else:
            return False
        connection.commit()
    except Exception as exception:
        print("Errortodo: ", exception)

def check_for_user_table():
    try:
        query = (''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='user' ''')
        execute_query = connection.execute(query).fetchone()
        if execute_query[0] == 1:
            return True
        else:
            return False
        connection.commit()
    except Exception as exception:
        print("Erroruser: ", exception)

def create_tables():
    try:
        tables = ["todo", "category", "user"]
        for table in tables:
            if table == "todo" and check_for_todo_table():
                pass
            elif table == "todo" and (check_for_todo_table() == False):
                connection.execute("CREATE TABLE todo (id INTEGER PRIMARY KEY, title char NOT NULL, content char NOT NULL, user_id INTEGER, category_id INTEGER, createdAt char)")
            elif table == "category" and check_for_category_table():
                pass
            elif table == "category" and (check_for_category_table() == False):
                connection.execute("CREATE TABLE category (id INTEGER PRIMARY KEY, name char NOT NULL, user_id INTEGER)")
            elif table == "user" and check_for_user_table():
                pass
            elif table == "user" and (check_for_user_table()  == False):
                connection.execute("CREATE TABLE user (id INTEGER PRIMARY KEY, username char NOT NULL)")
        connection.commit()
        print("Tables created")
    except Exception as exception:
        print("Error: ", exception)


# create_tables()