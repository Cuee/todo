from bottle import get, post, request, run, route, template, redirect, static_file, error
from user import add_user, get_username
from category import add_category, get_category_name
from todo import add_todo
from get_user import get_user_details as get_all_user_details
from user import get_user_id
from todo_create_table import create_tables
import json
import os

create_tables()
@route('/todo/login/', method=['POST'])
def get_user_details():
    try:
        username = request.forms.get('username')
        create_user = add_user(username)
        user_details = get_all_user_details(username)
        user_id = get_user_id(username)

        data =	{
                "user": user_details,
                "id": user_id,
                "message": "User Successfully added or created",
                "status": "OK",
                "success":  True
            }
        response = json.dumps(data)
        return response
    except Exception as exception:
        return exception

@get('/todo/get-category-name')
def get_category_name():
    try:
        category_id = request.forms.get('category_id')
        category_name = get_category_name(category_id)
        data =	{
                "name": category_name,
                "message": "Successful",
                "status": "OK",
                "success":  True
            }
        response = json.dumps(data)
        return response
    except Exception as exception:
        return exception

@route('/todo/add-category/', method='POST')
def add_category():
    try:
        user_id = request.forms.get('user_id')
        category_name = request.forms.get('category_name')
        username = get_username(user_id)
        add_new_category = add_category(username, category_name)
        if add_new_category == "Category Created":
            data =	{
                "message": "Category Created Successfully",
                "status": "OK",
                "success":  True
            }
            response = json.dumps(data)
            return response
        elif add_new_category == "Category Exists":
            data =	{
                "message": "Category Exists",
                "status": "Error",
                "success":  False
            }
            response = json.dumps(data)
            return response
        else:
            return "Error"
    except Exception as exception:
        return exception
        

@route('/todo/add-todo', method='POST')
def add_todo():
    try:
        user_id = request.forms.get('user_id')
        todo_content = request.forms.get('todo_content')
        todo_category = request.forms.get('todo_category')

        username = get_username(user_id)
        add_new_todo = add_todo(username, todo_category, todo_content)
        if add_new_todo == "Todo Created":
            data =	{
                "message": "Todo Created Successfully",
                "status": "OK",
                "success":  True
            }
            response = json.dumps(data)
            return response
        elif add_new_todo == "Invalid Todo":
            data =	{
                "message": "Invalid Todo",
                "status": "Error",
                "success":  False
            }
            response = json.dumps(data)
            return response
        else:
            return "Error"
    except Exception as exception:
        return exception


if os.environ.get('APP_LOCATION') == 'heroku':
    run(host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))
else:
    run(host='localhost', port=8080, debug=True)

