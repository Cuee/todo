from bottle import route, run, template
import sqlite3

def create_connection():
  try:
    connection = sqlite3.connect('todo.db')
    return connection
  except Exception as exception:
    print("Error: ", exception)


