import { Observable } from "tns-core-modules/data/observable";
import { DropDown } from "nativescript-drop-down";
import { Page } from "tns-core-modules/ui/page/page";

export class AddTodoModel extends Observable {

    private _page: Page;

    constructor(page) {
        super();
        this._page = page;
        this.createDropdown();
    }


    private createDropdown() {
        let categories = <DropDown>this._page.getViewById("categories");
        categories.items = ["ol", "one"];
       
    }
}
