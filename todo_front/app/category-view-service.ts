import { request, getFile, getImage, getJSON, getString } from "tns-core-modules/http";
import { Observable, Page } from "tns-core-modules/ui/page/page";
import { topmost } from "tns-core-modules/ui/frame/frame";
import * as appSettings from "tns-core-modules/application-settings";


export class CategoryViewService extends Observable {

    private _page: Page;

    constructor(page) {
        super();
        this._page = page;
    }

    
    sendRequest() {
        const categoryName = appSettings.getString("categoryName");
        const userId = appSettings.getNumber("user-id");
        alert(categoryName);
        alert(userId);
        console.dir(userId);
        request({
            url: "https://chitodo.herokuapp.com/todo/add-category/",
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Connection": "Keep-Alive",
                'Accept': 'application/json',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Allow-Headers': 'origin, content-type, accept',

            },
            content: JSON.stringify({
                category_name: categoryName,
                user_id: userId
            })
        })
            .then((response) => {
                const result = response.content.toJSON();
                console.dir(result)
                if (JSON.stringify(result) === "Category Created Successfully"){
                    topmost().navigate("add-category");
                }

            }, (error) => {
                alert(error)
                console.log(error);
            });
    }


    private updateMessage() {
    }
}

