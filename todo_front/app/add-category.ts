import { EventData, Page } from "tns-core-modules/ui/page/page";
import { AddCategoryModel } from "./add-category-model";
import {CategoryViewService} from "./category-view-service";

export function navigatingTo(args: EventData) {
    const page = <Page>args.object;
    page.bindingContext = new AddCategoryModel(page);
    page.bindingContext = new CategoryViewService(page);
    // let categories = <DropDown>page.getViewById("categories");
    // categories.items = ["ol", "one"];

}
