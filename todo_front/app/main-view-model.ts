import { Observable } from "tns-core-modules/data/observable";
import { topmost, Page } from "tns-core-modules/ui/frame/frame";
import { TextField } from "tns-core-modules/ui/text-field";
import { MainViewService } from "./main-view-service";
import * as appSettings from "tns-core-modules/application-settings";



export class MainViewModel extends Observable {

    private _username: string;
    private _page: Page;
    private _textFieldValue: string;
    private _service: MainViewService;

    constructor(page) {
        super();
        this._page = page;
        this._service = new MainViewService(page);
        this.updateMessage();
    }

    get username(): string {
        return this._username;
    }

    setUsername(username: string){
        if (username.length !==  0){
            this._username = username;
            return true;
        }
        else{
            return false;
        }
    }

    private setTextFieldValue(){
        let usernameTextField = <TextField>this._page.getViewById("usernameField");
        this._textFieldValue = usernameTextField.text;
    }

    private getTextFieldValue(){
        return this._textFieldValue;
    }

    signIn() {
        this.setTextFieldValue();
        let username = this.getTextFieldValue();
        if (this.setUsername(username)){
            appSettings.setString("username", username);
            this._service.sendRequest();
        }
        else{
            alert("Username cannot be empty");
        }
    }

    private updateMessage() {
    }
}


//         onReturnPress(args) {
//             // returnPress event will be triggered when user submits a value
//             const textField: TextField = <TextField>args.object;
//             // Gets or sets the placeholder text.
//             console.log(textField.hint);
//             // Gets or sets the input text.
//             alert(textField.text);
//             // Gets or sets the secure option (e.g. for passwords).
//             console.log(textField.secure);

//             // Gets or sets the soft keyboard type. Options: "datetime" | "phone" | "number" | "url" | "email"
//             console.log(textField.keyboardType);
//             // Gets or sets the soft keyboard return key flavor. Options: "done" | "next" | "go" | "search" | "send"
//             console.log(textField.returnKeyType);
//             // Gets or sets the autocapitalization type. Options: "none" | "words" | "sentences" | "allcharacters"
//             console.log(textField.autocapitalizationType);

//             // Gets or sets a value indicating when the text property will be updated.
//             console.log(textField.updateTextTrigger);
//             // Gets or sets whether the instance is editable.
//             console.log(textField.editable);
//             // Enables or disables autocorrection.
//             console.log(textField.autocorrect);
//             // Limits input to a certain number of characters.
//             console.log(textField.maxLength);

//         }

//         onFocus(args): void {
//             // focus event will be triggered when the users enters the TextField
//             console.log("onFocus event");
//             alert("Hey");
//         }

//         onBlur(args) {
//             // blur event will be triggered when the user leaves the TextField
//             const textField: TextField = <TextField>args.object;
//             console.log("onBlur event");
//         }

//       

//     }
// }
