import { Observable } from "tns-core-modules/data/observable";;
import { Page } from "tns-core-modules/ui/page/page";
import { ObservableArray } from "tns-core-modules/data/observable-array";
import * as appSettings from "tns-core-modules/application-settings";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { CategoryViewService } from "./category-view-service";

export class AddCategoryModel extends Observable {

    private _page: Page;
    private _categoryName: string;
    private _textFieldValue: string;
    private _service: CategoryViewService;


    constructor(page) {
        super();
        this._page = page;
        this ._service = new CategoryViewService(page);
        // Initialize default values.
        this.updateMessage();
    }

    get categoryName(): string {
        return this._categoryName;
    }

    setCategoryName(categoryName: string) {
        if (categoryName.length !== 0) {
            this._categoryName = categoryName;
            return true;
        }
        else {
            return false;
        }
    }

    private setTextFieldValue() {
        let usernameTextField = <TextField>this._page.getViewById("categoryName");
        this._textFieldValue = usernameTextField.text;
    }

    private getTextFieldValue() {
        return this._textFieldValue;
    }

    addCategory() {
        this.setTextFieldValue();
        let categoryName = this.getTextFieldValue();
        if (this.setCategoryName(categoryName)) {
            alert(categoryName)
            appSettings.setString("categoryName", categoryName);
            this._service.sendRequest();
        }
        else {
            alert("Name of category cannot be empty");
        }
    }

    onListViewLoaded() {
        let myCategories = JSON.parse(appSettings.getString("user-categories", "[]"));
        let viewModel = new Observable();
        let myItems = new ObservableArray();
        myCategories.forEach(element => {
            alert(element);
            viewModel.set("myCategories", myItems);
            myItems.push({ category: element });
        });
        this._page.bindingContext = viewModel;
    }



    private updateMessage() {

    }
}
