import { request, getFile, getImage, getJSON, getString } from "tns-core-modules/http";
import { Observable } from "tns-core-modules/ui/page/page";
import { topmost, Page } from "tns-core-modules/ui/frame/frame";
import { CategoryViewService } from "./category-view-service";
import * as appSettings from "tns-core-modules/application-settings";


export class MainViewService extends Observable {
    private _page: Page;
    constructor(page) {
        super();
        this._page = page;
    }

    sendRequest() {
        const username = appSettings.getString("username");
        request({
            url: "https://chitodo.herokuapp.com/todo/login/",
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Connection": "Keep-Alive",
                'Accept': 'application/json',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Allow-Headers': 'origin, content-type, accept',

            },
            content: JSON.stringify({
                username: username
            })
        })
            .then((response) => {
                const result = response.content.toJSON();
                alert(JSON.stringify(result))
                appSettings.setString("user-categories", JSON.stringify(result.user[0]));
                appSettings.setString("user-todos", JSON.stringify(result.user[1]));
                appSettings.setNumber("user-id", result.id);
                topmost().navigate("todo-page");
                

            }, (error) => {
                alert(error)
                console.log(error);
            });
    }

}

