import { Observable } from "tns-core-modules/data/observable";
import { Page, topmost } from "tns-core-modules/ui/frame/frame";
import { Menu } from "nativescript-menu";

export class TodoViewModel extends Observable {

    private _page: Page;
   
    constructor(page) {
        super();
        this._page = page;
    }

    add() {
        Menu.popup({
            view: this._page.getViewById("menuIcon"),
            actions: [{ id: 1, title: "Add Category" }, { id: 2, title: "Add Todo" }]
        })
            .then(action => {
                this.performMenuAction(action.id);
            })
            .catch(console.log);
    }

    private performMenuAction(actionId) {
        if (actionId === 1) {
            topmost().navigate("add-category");
        }
        else if (actionId === 2) {
            topmost().navigate("add-todo");
        }
    }
}
