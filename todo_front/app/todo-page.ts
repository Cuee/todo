import { EventData, Page } from "tns-core-modules/ui/page/page";
import { TodoViewModel } from "./todo-view-model";


// Event handler for Page "navigatingTo" event attached in todo-page.xml
export function navigatingTo(args: EventData) {
    const page = <Page>args.object;
    page.bindingContext = new TodoViewModel(page);
    // let categories = <DropDown>page.getViewById("categories");
    // categories.items = ["ol", "one"];

}
