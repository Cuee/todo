import { EventData, Page } from "tns-core-modules/ui/page/page";
import { AddTodoModel } from "./add-todo-model";

export function navigatingTo(args: EventData) {
    const page = <Page>args.object;
    page.bindingContext = new AddTodoModel(page);

}
